from flask import Flask, render_template, request
import pandas as pd
import pickle

app = Flask(__name__)

model = pickle.load(open("PipeModel.pkl", "rb"))

name = ["name", 'production_year', 'km_driven', 'fuel', 'seller_type', "gear_box", "owner", 'mileage', 'engine', 'max_power', "torque", 'seats']

df = pd.read_csv('car_datsets.csv')

# Extract unique values for the relevant columns
unique_names = df['name'].unique()
unique_fuels = df['fuel'].unique()
unique_seller_types = df['seller_type'].unique()
unique_transmissions = df['gear_box'].unique()
unique_owners = df['owner'].unique()
unique_torques = df['torque'].unique()

@app.route("/")
def home():
    return render_template("output.html")

@app.route("/index")
def index():
    return render_template("index.html", 
            unique_names=unique_names, 
                        unique_fuels=unique_fuels, 
                        unique_seller_types=unique_seller_types, 
                        unique_transmissions=unique_transmissions, 
                        unique_owners=unique_owners, 
                        unique_torques=unique_torques
                        )

@app.route("/predict",methods=['POST'])
def prediction():
    if request.method == 'POST':
        col1 = request.form['col1']
        col2 = float(request.form['col2'])
        col3 = float(request.form['col3'])
        col4 = request.form['col4']
        col5 = request.form['col5']
        col6 = request.form['col6']
        col7 = request.form['col7']
        col8 = float(request.form['col8'])
        col9 = float(request.form['col9'])
        col10 = float(request.form['col10'])
        col11 = request.form['col11']
        col12 = float(request.form['col12'])

        x_sample = [[col1,col2,col3,col4,col5,col6,col7,col8, col9, col10, col11, col12]]
        X = pd.DataFrame(x_sample,columns=name)

        result = model.predict(X)
        return render_template("index.html",value=result)